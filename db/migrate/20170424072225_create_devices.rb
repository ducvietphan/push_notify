class CreateDevices < ActiveRecord::Migration[5.0]
  def change
    create_table :devices do |t|

      t.string :platform, null: false
      t.string :token, null: false
      t.timestamps null: false
    end
  end
end
