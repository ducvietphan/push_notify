require 'gcm'

class AndroidsController < ApplicationController
  def send_notify
  end
  
  def push_notify
    gcm = GCM.new('AIzaSyAXmqAL_Y9Zrugd8DDo6o4QnEZZ9nLEkbE')
    response = gcm.send_with_notification_key(params[:token], {data: {message: params[:message]}})
    redirect_to send_notify_android_path
  end
end
