class DevicesController < ApplicationController
  def index
    @devices = Device.all
  end
  
  def new
    device = Device.new(token: params[:token], platform: params[:platform])
    device.save
    head :ok
  end
end
