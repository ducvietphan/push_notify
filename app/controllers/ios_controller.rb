require 'houston'

class IosController < ApplicationController
  def send_notify
  end
  
  def push_notify
    apn = Houston::Client.production if Rails.env.production?
    apn = Houston::Client.development if Rails.env.development?
      
    apn.certificate = File.read(File.join(Rails.root, 'public','ck.pem'))
    apn.passphrase = '1234'
    
    token = params[:token]
    
    notification = Houston::Notification.new(device: token)
    notification.alert = params[:messeage]
    
    notification.badge = 57
    notification.sound = 'sosumi.aiff'
    notification.category = 'INVITE_CATEGORY'
    notification.content_available = true
    notification.mutable_content = true
    notification.custom_data = { foo: 'bar' }
    
    apn.push(notification)
    redirect_to root_path
  end
end
