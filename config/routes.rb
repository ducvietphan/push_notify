Rails.application.routes.draw do
  root to: 'devices#index'
  resource :devices, only: :new
  resource :ios, only: :none do
    collection do
      get :send_notify
      post :push_notify
    end
  end
  resource :android, only: :none do
    collection do
      get :send_notify
      post :push_notify
    end
  end
end
